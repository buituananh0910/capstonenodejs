/**
 * @swagger
 *  /api/auth/signup:
 *  post:
 *      description: responses
 *      tags: [Auth]
 *      parameters:
 *      - in: body
 *        name: model
 *        schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             name:
 *               type: string
 *             email:
 *               type: string
 *             password:
 *               type: string
 *             phone:
 *               type: string
 *             birthday:
 *               type: string
 *             gender:
 *               type: boolean
 *             role:
 *               type: string
 *      responses:
 *          200:
 *              description: res
 */

/**
 * @swagger
 * /api/auth/signin:
 *  post:
 *      description: responses
 *      tags: [Auth]
 *      parameters:
 *      - in: body
 *        name: model
 *        schema:
 *           type: object
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/binh-luan:
 *  get:
 *      description: responses
 *      tags: [BinhLuan]
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/binh-luan:
 *  post:
 *      description: responses
 *      tags: [BinhLuan]
 *      parameters:
 *      - in: body
 *        name: model
 *        schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             maphong:
 *               type: integer
 *             manguoibinhluan:
 *               type: integer
 *             ngaybinhluan:
 *               type: integer
 *             noidung:
 *               type: string
 *             saobinhluan:
 *               type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/binh-luan/{id}:
 *  put:
 *      description: responses
 *      tags: [BinhLuan]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: path
 *        name: id
 *        type: integer
 *      - in: body
 *        name: model
 *        schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             maphong:
 *               type: integer
 *             manguoibinhluan:
 *               type: integer
 *             ngaybinhluan:
 *               type: integer
 *             noidung:
 *               type: string
 *             saobinhluan:
 *               type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/binh-luan/{id}:
 *  delete:
 *      description: responses
 *      tags: [BinhLuan]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: path
 *        name: id
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/binh-luan/lay-binh-luan-theo-phong/{maphong}:
 *  get:
 *      description: responses
 *      tags: [BinhLuan]
 *      parameters:
 *      - in: path
 *        name: maphong
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/dat-phong:
 *  get:
 *      description: responses
 *      tags: [DatPhong]
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/dat-phong:
 *  post:
 *      description: responses
 *      tags: [DatPhong]
 *      parameters:
 *      - in: body
 *        name: model
 *        schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             maphong:
 *               type: integer
 *             ngayden:
 *               type: integer
 *             ngaydi:
 *               type: integer
 *             soluongkhach:
 *               type: integer
 *             manguoidung:
 *               type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/dat-phong/{id}:
 *  get:
 *      description: responses
 *      tags: [DatPhong]
 *      parameters:
 *      - in: path
 *        name: id
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/dat-phong/{id}:
 *  put:
 *      description: responses
 *      tags: [DatPhong]
 *      parameters:
 *      - in: path
 *        name: id
 *        type: integer
 *      - in: body
 *        name: model
 *        schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             maphong:
 *               type: integer
 *             ngayden:
 *               type: integer
 *             ngaydi:
 *               type: integer
 *             soluongkhach:
 *               type: integer
 *             manguoidung:
 *               type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/dat-phong/{id}:
 *  delete:
 *      description: responses
 *      tags: [DatPhong]
 *      parameters:
 *      - in: path
 *        name: id
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/dat-phong/lay-theo-nguoi-dung/{manguoidung}:
 *  get:
 *      description: responses
 *      tags: [DatPhong]
 *      parameters:
 *      - in: path
 *        name: manguoidung
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users:
 *  get:
 *      description: responses
 *      tags: [NguoiDung]
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users:
 *  post:
 *      description: responses
 *      tags: [NguoiDung]
 *      parameters:
 *      - in: body
 *        name: model
 *        schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             name:
 *               type: string
 *             email:
 *               type: string
 *             password:
 *               type: string
 *             phone:
 *               type: string
 *             birthday:
 *               type: string
 *             gender:
 *               type: boolean
 *             role:
 *               type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users:
 *  delete:
 *      description: responses
 *      tags: [NguoiDung]
 *      parameters:
 *      - in: query
 *        name: id
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users/phan-trang-tim-kiem:
 *  get:
 *      description: responses
 *      tags: [NguoiDung]
 *      parameters:
 *      - in: query
 *        name: pageindex
 *        type: integer
 *      - in: query
 *        name: pagesize
 *        type: integer
 *      - in: query
 *        name: keyword
 *        type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users/{id}:
 *  get:
 *      description: responses
 *      tags: [NguoiDung]
 *      parameters:
 *      - in: path
 *        name: id
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users/{id}:
 *  put:
 *      description: responses
 *      tags: [NguoiDung]
 *      parameters:
 *      - in: path
 *        name: id
 *        type: integer
 *      - in: body
 *        name: model
 *        schema:
 *           properties:
 *             id:
 *               type: integer
 *             name:
 *               type: string
 *             email:
 *               type: string
 *             phone:
 *               type: string
 *             birthday:
 *               type: string
 *             gender:
 *               type: boolean
 *             role:
 *               type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users/search/{tennguoidung}:
 *  get:
 *      description: responses
 *      tags: [NguoiDung]
 *      parameters:
 *      - in: path
 *        name: tennguoidung
 *        type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/users/upload-avatar:
 *  post:
 *      description: responses
 *      tags: [NguoiDung]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: formData
 *        name: file
 *        type: file
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue:
 *  get:
 *      description: responses
 *      tags: [Phong]
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue:
 *  post:
 *      description: responses
 *      tags: [Phong]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: body
 *        name: model
 *        schema:
 *           properties:
 *             id:
 *               type: integer
 *             tenphong:
 *               type: string
 *             khach:
 *               type: integer
 *             phongngu:
 *               type: integer
 *             giuong:
 *               type: integer
 *             phongtam:
 *               type: integer
 *             mota:
 *               type: string
 *             giatien:
 *               type: integer
 *             maygiat:
 *               type: boolean
 *             banla:
 *               type: boolean
 *             tivi:
 *               type: boolean
 *             dieuhoa:
 *               type: boolean
 *             wifi:
 *               type: boolean
 *             bep:
 *               type: boolean
 *             doxe:
 *               type: boolean
 *             hoboi:
 *               type: boolean
 *             banui:
 *               type: boolean
 *             mavitri:
 *               type: integer
 *             hinhanh:
 *               type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue/lay-phong-theo-vi-tri:
 *  get:
 *      description: responses
 *      tags: [Phong]
 *      parameters:
 *      - in: query
 *        name: mavitri
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue/phan-trang-tim-kiem:
 *  get:
 *      description: responses
 *      tags: [Phong]
 *      parameters:
 *      - in: query
 *        name: pageindex
 *        type: integer
 *      - in: query
 *        name: pagesize
 *        type: integer
 *      - in: query
 *        name: keyword
 *        type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue/{id}:
 *  get:
 *      description: responses
 *      tags: [Phong]
 *      parameters:
 *      - in: path
 *        name: id
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue/{id}:
 *  put:
 *      description: responses
 *      tags: [Phong]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: path
 *        name: id
 *        type: integer
 *      - in: body
 *        name: model
 *        schema:
 *           properties:
 *             id:
 *               type: integer
 *             tenphong:
 *               type: string
 *             khach:
 *               type: integer
 *             phongngu:
 *               type: integer
 *             giuong:
 *               type: integer
 *             phongtam:
 *               type: integer
 *             mota:
 *               type: string
 *             giatien:
 *               type: integer
 *             maygiat:
 *               type: boolean
 *             banla:
 *               type: boolean
 *             tivi:
 *               type: boolean
 *             dieuhoa:
 *               type: boolean
 *             wifi:
 *               type: boolean
 *             bep:
 *               type: boolean
 *             doxe:
 *               type: boolean
 *             hoboi:
 *               type: boolean
 *             banui:
 *               type: boolean
 *             mavitri:
 *               type: integer
 *             hinhanh:
 *               type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue/{id}:
 *  delete:
 *      description: responses
 *      tags: [Phong]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: path
 *        name: id
 *        type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/phong-thue/upload-hinh-phong:
 *  post:
 *      description: responses
 *      tags: [Phong]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: query
 *        name: maphong
 *        type: string
 *      - in: formData
 *        name: fromFile
 *        type: file
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/vi-tri:
 *  get:
 *      description: responses
 *      tags: [ViTri]
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/vi-tri:
 *  post:
 *      description: responses
 *      tags: [ViTri]
 *      parameters:
 *      - in: header
 *        name: token
 *        type: string
 *      - in: body
 *        name: model
 *        schema:
 *           properties:
 *             id:
 *               type: integer
 *             tenvitri:
 *               type: string
 *             tinhthanh:
 *               type: string
 *             quocgia:
 *               type: integer
 *             hinhanh:
 *               type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/vi-tri/phan-trang-tim-kiem:
 *  get:
 *      description: responses
 *      tags: [ViTri]
 *      parameters:
 *      - in: query
 *        name: pageindex
 *        type: integer
 *      - in: query
 *        name: pagesize
 *        type: integer
 *      - in: query
 *        name: keyword
 *        type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/vi-tri/{id}:
 *  get:
 *      description: responses
 *      tags: [ViTri]
 *      parameters:
 *        - in: path
 *          name: id
 *          type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/vi-tri/{id}:
 *  put:
 *      description: responses
 *      tags: [ViTri]
 *      parameters:
 *        - in: header
 *          name: token
 *          type: string
 *        - in: path
 *          name: id
 *          type: integer
 *        - in: body
 *          name: model
 *          schema:
 *             properties:
 *               id:
 *                 type: integer
 *               tenvitri:
 *                 type: string
 *               tinhthanh:
 *                 type: string
 *               quocgia:
 *                 type: integer
 *               hinhanh:
 *                 type: string
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/vi-tri/{id}:
 *  delete:
 *      description: responses
 *      tags: [ViTri]
 *      parameters:
 *        - in: header
 *          name: token
 *          type: string
 *        - in: path
 *          name: id
 *          type: integer
 *      responses:
 *          200:
 *              description: success
 */

/**
 * @swagger
 * /api/vi-tri/upload-hinh-vitri:
 *  post:
 *      description: responses
 *      tags: [ViTri]
 *      parameters:
 *        - in: header
 *          name: token
 *          type: string
 *        - in: query
 *          name: mavitri
 *          type: integer
 *        - in: formData
 *          name: formFile
 *          type: file
 *      responses:
 *          200:
 *              description: success
 */
