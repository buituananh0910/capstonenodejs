// yarn init
// yarn add express dotenv nodemon

const express = require("express");
const app = express();
app.use(express.json());
app.use(express.static("."));
app.listen(8080);

const { PrismaClient } = require("@prisma/client");
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const prisma = new PrismaClient();

app.post("/api/auth/signup", async (req, res) => {
  let { id, name, email, password, phone, birthday, gender, role } = req.body;
  let pass_word = password;
  let birth_day = birthday;
  await prisma.nguoidung.create({
    data: { name, email, pass_word, phone, birth_day, gender, role },
  });
});

app.post("/api/auth/signin", async (req, res) => {
  let { email, password } = req.body;
  let data = await prisma.nguoidung.findFirst({
    where: {
      email,
      pass_word: password,
    },
  });
  console.log(data);
  if (data) {
    res.send("login success");
  } else {
    res.send("login fail");
  }
});

app.get("/api/binh-luan", async (req, res) => {
  let data = await prisma.binhluan.findMany();
  res.send(data);
});

app.post("/api/binh-luan", async (req, res) => {
  let { id, maphong, manguoibinhluan, ngaybinhluan, noidung, saobinhluan } =
    req.body;
  let ma_phong = maphong;
  let ma_nguoi_binh_luan = manguoibinhluan;
  let ngay_binh_luan = ngaybinhluan;
  let noi_dung = noidung;
  let sao_binh_luan = saobinhluan;
  await prisma.binhluan.create({
    data: {
      ma_nguoi_binh_luan,
      ngay_binh_luan,
      noi_dung,
      sao_binh_luan,
      ma_phong,
    },
  });
});

app.put("/api/binh-luan/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let { maphong, manguoibinhluan, ngaybinhluan, noidung, saobinhluan } =
    req.body;
  let ma_phong = maphong;
  let ma_nguoi_binh_luan = manguoibinhluan;
  let ngay_binh_luan = ngaybinhluan;
  let noi_dung = noidung;
  let sao_binh_luan = saobinhluan;
  await prisma.binhluan.update({
    data: {
      ma_nguoi_binh_luan,
      ngay_binh_luan,
      noi_dung,
      sao_binh_luan,
      ma_phong,
    },
    where: { id },
  });
});

app.delete("/api/binh-luan/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  await prisma.binhluan.delete({
    where: { id },
  });
});

app.get(
  "/api/binh-luan/lay-binh-luan-theo-phong/:maphong",
  async (req, res) => {
    let { maphong } = req.params;
    let ma_phong = +maphong;
    let data = await prisma.binhluan.findMany({
      where: {
        ma_phong,
      },
    });
    res.send(data);
  }
);

app.get("/api/dat-phong", async (req, res) => {
  let data = await prisma.datphong.findMany();
  res.send(data);
});

app.post("/api/dat-phong", async (req, res) => {
  let { id, maphong, ngayden, ngaydi, soluongkhach, manguoidung } = req.body;
  let ma_phong = maphong;
  let ngay_den = ngayden;
  let ngay_di = ngaydi;
  let so_luong_khach = soluongkhach;
  let ma_nguoi_dat = manguoidung;
  await prisma.datphong.create({
    data: { so_luong_khach, ma_nguoi_dat, ma_phong, ngay_den, ngay_di },
  });
});

app.get("/api/dat-phong/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let data = await prisma.datphong.findMany({
    where: { id },
  });
  res.send(data);
});

app.put("/api/dat-phong/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let { maphong, ngayden, ngaydi, soluongkhach, manguoidung } = req.body;
  let ma_phong = maphong;
  let ngay_den = ngayden;
  let ngay_di = ngaydi;
  let so_luong_khach = soluongkhach;
  let ma_nguoi_dat = manguoidung;
  await prisma.datphong.update({
    data: { so_luong_khach, ma_nguoi_dat, ma_phong, ngay_den, ngay_di },
    where: { id },
  });
});

app.delete("/api/dat-phong/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  await prisma.datphong.delete({
    where: { id },
  });
});

app.get("/api/dat-phong/lay-theo-nguoi-dung/:manguoidung", async (req, res) => {
  let { manguoidung } = req.params;
  let ma_nguoi_dat = +manguoidung;
  let data = await prisma.datphong.findMany({
    where: { ma_nguoi_dat },
  });
  res.send(data);
});

app.get("/api/users", async (req, res) => {
  let data = await prisma.nguoidung.findMany();
  res.send(data);
});

app.post("/api/users", async (req, res) => {
  let { id, name, email, password, phone, birthday, gender, role } = req.body;
  let pass_word = password;
  let birth_day = birthday;
  await prisma.nguoidung.create({
    data: { name, email, pass_word, phone, birth_day, gender, role },
  });
});

app.delete("/api/users", async (req, res) => {
  let { id } = req.query;
  id = +id;
  await prisma.nguoidung.delete({
    where: { id },
  });
});

app.get("/api/users/phan-trang-tim-kiem", async (req, res) => {
  let { keyword } = req.query;
  let data = await prisma.nguoidung.findMany({
    where: {
      name: {
        contains: keyword,
      },
    },
  });
  res.send(data);
});

app.get("/api/users/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let data = await prisma.nguoidung.findMany({
    where: {
      id,
    },
  });
  res.send(data);
});

app.put("/api/users/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let { name, email, phone, birthday, gender, role } = req.body;
  let birth_day = birthday;
  await prisma.nguoidung.update({
    data: { name, email, phone, birth_day, gender, role },
    where: { id },
  });
});

app.get("/api/users/search/:tennguoidung", async (req, res) => {
  let { tennguoidung } = req.params;
  let name = tennguoidung;
  let data = await prisma.nguoidung.findMany({
    where: {
      name,
    },
  });
  res.send(data);
});

app.get("/api/phong-thue", async (req, res) => {
  let data = await prisma.phong.findMany();
  res.send(data);
});

app.post("/api/phong-thue", async (req, res) => {
  let {
    tenphong,
    khach,
    phongngu,
    giuong,
    phongtam,
    mota,
    giatien,
    maygiat,
    banla,
    tivi,
    dieuhoa,
    wifi,
    bep,
    doxe,
    hoboi,
    banui,
    mavitri,
    hinhanh,
  } = req.body;
  khach = +khach;
  giuong = +giuong;
  let ten_phong = tenphong;
  let phong_ngu = +phongngu;
  let phong_tam = +phongtam;
  let mo_ta = mota;
  let gia_tien = +giatien;
  let may_giat = maygiat;
  let ban_la = banla;
  let dieu_hoa = dieuhoa;
  let do_xe = doxe;
  let ho_boi = hoboi;
  let ban_ui = banui;
  let hinh_anh = hinhanh;
  let vitriId = +mavitri;
  await prisma.phong.create({
    data: {
      ten_phong,
      khach,
      phong_ngu,
      phong_tam,
      mo_ta,
      gia_tien,
      may_giat,
      ban_la,
      tivi,
      dieu_hoa,
      wifi,
      bep,
      do_xe,
      ho_boi,
      ban_ui,
      hinh_anh,
      vitriId,
      giuong,
    },
  });
});

app.get("/api/phong-thue/lay-phong-theo-vi-tri", async (req, res) => {
  let { mavitri } = req.query;
  let vitriId = +mavitri;
  let data = await prisma.phong.findMany({
    where: { vitriId },
  });
  res.send(data);
});

app.get("/api/phong-thue/phan-trang-tim-kiem", async (req, res) => {
  let { keyword } = req.query;
  let data = await prisma.phong.findMany({
    where: {
      ten_phong: {
        contains: keyword,
      },
    },
  });
  res.send(data);
});

app.get("/api/phong-thue/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let data = await prisma.phong.findMany({
    where: {
      id,
    },
  });
  res.send(data);
});

app.put("/api/phong-thue/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let {
    tenphong,
    khach,
    phongngu,
    giuong,
    phongtam,
    mota,
    giatien,
    maygiat,
    banla,
    tivi,
    dieuhoa,
    wifi,
    bep,
    doxe,
    hoboi,
    banui,
    mavitri,
    hinhanh,
  } = req.body;
  khach = +khach;
  giuong = +giuong;
  let ten_phong = tenphong;
  let phong_ngu = +phongngu;
  let phong_tam = +phongtam;
  let mo_ta = mota;
  let gia_tien = +giatien;
  let may_giat = maygiat;
  let ban_la = banla;
  let dieu_hoa = dieuhoa;
  let do_xe = doxe;
  let ho_boi = hoboi;
  let ban_ui = banui;
  let hinh_anh = hinhanh;
  let vitriId = +mavitri;
  await prisma.phong.update({
    data: {
      ten_phong,
      khach,
      phong_ngu,
      phong_tam,
      mo_ta,
      gia_tien,
      may_giat,
      ban_la,
      tivi,
      dieu_hoa,
      wifi,
      bep,
      do_xe,
      ho_boi,
      ban_ui,
      hinh_anh,
      vitriId,
      giuong,
    },
    where: { id },
  });
});

app.delete("/api/phong-thue/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  await prisma.phong.delete({
    where: {
      id,
    },
  });
});

app.get("/api/vi-tri", async (req, res) => {
  let data = await prisma.vitri.findMany();
  res.send(data);
});

app.post("/api/vi-tri", async (req, res) => {
  let { tenvitri, tinhthanh, quocgia, hinhanh } = req.body;
  let ten_vi_tri = tenvitri;
  let tinh_thanh = tinhthanh;
  let quoc_gia = +quocgia;
  let hinh_anh = hinhanh;
  await prisma.vitri.create({
    data: { ten_vi_tri, tinh_thanh, quoc_gia, hinh_anh },
  });
});

app.get("/api/vi-tri/phan-trang-tim-kiem", async (req, res) => {
  let { keyword } = req.query;
  let data = await prisma.vitri.findMany({
    where: {
      ten_vi_tri: {
        contains: keyword,
      },
    },
  });
  res.send(data);
});

app.get("/api/vi-tri/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let data = await prisma.vitri.findMany({
    where: {
      id,
    },
  });
  res.send(data);
});

app.put("/api/vi-tri/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  let { tenvitri, tinhthanh, quocgia, hinhanh } = req.body;
  let ten_vi_tri = tenvitri;
  let tinh_thanh = tinhthanh;
  let quoc_gia = +quocgia;
  let hinh_anh = hinhanh;
  await prisma.vitri.update({
    data: { ten_vi_tri, tinh_thanh, quoc_gia, hinh_anh },
    where: {
      id,
    },
  });
});

app.delete("/api/vi-tri/:id", async (req, res) => {
  let { id } = req.params;
  id = +id;
  await prisma.vitri.delete({
    where: { id },
  });
});

// swagger
const options = {
  definition: {
    info: {
      title: "api",
      version: "1.0.0",
    },
  },
  apis: ["swagger/index.js"],
};

const specs = swaggerJSDoc(options);

app.use("/", swaggerUi.serve, swaggerUi.setup(specs));
